import chalk from 'chalk'

export const WARN = 'warn'
export const ERROR = 'error'
export const DEBUG = 'debug'
export const INFO = 'info'
export const UPDATE = 'update'

// export const log = (message, level = DEBUG) => {
//   switch (level) {
//     case ERROR:
//       console.log(chalk.red(message))
//       break
//     case WARN:
//       console.log(chalk.yellow(message))
//       break
//     case INFO:
//       console.log(chalk.white(message))
//       break
//     case DEBUG:
//       console.log(chalk.green(message))
//       break
//     case UPDATE:
//       process.stdout.write('\r' + chalk.bgWhite.black(message))
//       break
//     default:
//       console.log(chalk.green(message))
//   }
// }

class Log {
  private lastUpdate = false

  public log = (message, level = DEBUG) => {
    if (level !== UPDATE && this.lastUpdate) {
      console.log('\n')
      this.lastUpdate = false
    }
    switch (level) {
      case ERROR:
        console.log(chalk.red(message))
        break
      case WARN:
        console.log(chalk.yellow(message))
        break
      case INFO:
        console.log(chalk.white(message))
        break
      case DEBUG:
        console.log(chalk.green(message))
        break
      case UPDATE:
        this.lastUpdate = true
        process.stdout.write('\r' + chalk.bgWhite.black(message))
        break
      default:
        console.log(chalk.green(message))
    }
  }
}

export default Log
