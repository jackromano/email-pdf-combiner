"use strict";
/*
Email PDF Combiner
Jack Romano (c) 2021
*/
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pdf_merge_1 = __importDefault(require("pdf-merge"));
const config_1 = __importDefault(require("config"));
const ImapFunctions_1 = __importDefault(require("./ImapFunctions"));
const SmtpFunctions_1 = __importDefault(require("./SmtpFunctions"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const docx_pdf_1 = __importDefault(require("docx-pdf"));
const chalk_1 = __importDefault(require("chalk"));
const log_1 = __importStar(require("./log"));
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        let intervalID;
        const Logger = new log_1.default();
        const log = Logger.log;
        console.log(chalk_1.default.bgWhite.black(`
╔════════════════════════╗
║   ${chalk_1.default.bold('Email PDF Combiner')}   ║
║  Jack Romano (c) 2022  ║
╚════════════════════════╝\n
`));
        if (config_1.default.get('general.codeOn')) {
            log(`${chalk_1.default.red('SECURITY CODE ON!')} For security, only emails with the code "${chalk_1.default.red(config_1.default.get('email.code'))}" in the subject line will be processed. \n${chalk_1.default.dim('To disable this feature, set "codeOn" to ' + chalk_1.default.italic('false') + ' in default.json.\n')}`);
        }
        // Handle SIGINT
        process.on('SIGINT', () => {
            log('Closing...', log_1.WARN);
            clearInterval(intervalID);
        });
        const imapFunctions = new ImapFunctions_1.default({
            address: config_1.default.get('email.address'),
            password: config_1.default.get('email.password'),
            server: config_1.default.get('imap.server'),
            port: config_1.default.get('imap.port'),
            attachmentFolder: config_1.default.get('general.attachmentFolder'),
            validExtensions: config_1.default.get('general.validExtensions'),
            codeOn: config_1.default.get('general.codeOn'),
            code: config_1.default.get('email.code')
        }, log);
        const smtpFunctions = new SmtpFunctions_1.default({
            host: config_1.default.get('smtp.server'),
            port: config_1.default.get('smtp.port'),
            address: config_1.default.get('email.address'),
            password: config_1.default.get('email.password')
        });
        const emailDelay = config_1.default.get('general.checkEmailDelay') * 1000;
        const restartLoop = () => {
            intervalID = setTimeout(mainLoop, emailDelay);
        };
        const emptyFolder = folderPath => {
            fs_1.default.readdir(folderPath, (err, files) => {
                if (err)
                    throw err;
                for (const file of files) {
                    fs_1.default.unlink(path_1.default.join(folderPath, file), err => {
                        if (err)
                            throw err;
                    });
                }
            });
        };
        const mergePDFs = (files, fileName) => __awaiter(this, void 0, void 0, function* () {
            return (0, pdf_merge_1.default)(files, { output: fileName });
        });
        const handleDocx = (resolve, reject) => (err, result) => {
            if (err) {
                log('Error converting DOCX to PDF.', log_1.ERROR);
                reject(err);
            }
            else {
                log('Converted DOCX to PDF.', log_1.INFO);
                resolve(result);
            }
        };
        // Main Loop
        const mainLoop = () => __awaiter(this, void 0, void 0, function* () {
            // Check email
            const messagesData = yield imapFunctions.fetchEmail();
            const emailsProcessed = messagesData.messages.length;
            if (emailsProcessed === 0) {
                const now = new Date();
                log(` [${now.toLocaleTimeString()}] No emails found. Waiting... `, log_1.UPDATE);
            }
            else {
                log(`${messagesData.messages.length} ${messagesData.messages.length === 1 ? 'message' : 'messages'} received.`, log_1.INFO);
                const pdfPromises = [];
                for (let index = 0; index < messagesData.messages.length; index++) {
                    let resultMessage = '';
                    const message = messagesData.messages[index];
                    const pdfsToMerge = messagesData.attachments.filter(attachment => {
                        return attachment.id === message.id &&
                            config_1.default.get('general.validExtensions')
                                .some(extension => attachment.filename.includes(extension));
                    }).map(attachment => attachment.filename);
                    // Handle emails with no attachments.
                    if (pdfsToMerge.length === 0) {
                        log('No attachments found in email. Sending email and skipping', log_1.WARN);
                        yield smtpFunctions.sendEmail({
                            to: message.from,
                            subject: `No Attachments: ${message.subject}`,
                            message: 'No attachments were found in this email.'
                        });
                    }
                    else {
                        for (let index = 0; index < pdfsToMerge.length; index++) {
                            // pdfsToMerge.forEach((attachmentFilename, index) => {
                            const attachmentFilename = pdfsToMerge[index];
                            if (attachmentFilename &&
                                attachmentFilename.includes('.docx')) {
                                const newFilename = `${attachmentFilename}.pdf`;
                                // convert the docx to pdf
                                yield new Promise((resolve, reject) => {
                                    (0, docx_pdf_1.default)(attachmentFilename, newFilename, handleDocx(resolve, reject));
                                });
                                // replace  the docx filename with the pdf name
                                pdfsToMerge[index] = newFilename;
                                resultMessage += `\nConverted ${attachmentFilename} to PDF.`;
                            }
                        }
                        // Handle emails with attachments
                        log(`Merging ${pdfsToMerge.length} files in email ${index +
                            1} of ${emailsProcessed}`, log_1.INFO);
                        resultMessage += `\nMerged ${pdfsToMerge.length} files in email.`;
                        // Merge PDFs
                        const pdfPromise = new Promise((resolve, reject) => {
                            mergePDFs(pdfsToMerge, `${config_1.default.get('general.outputFolder')}${message.id}.pdf`).then(() => {
                                // Send PDF
                                const subject = `MERGED: ${message.subject}`;
                                log(`Sending message ${index +
                                    1} of ${emailsProcessed} (${chalk_1.default.italic(subject)})...`, log_1.INFO);
                                return smtpFunctions.sendEmail({
                                    to: message.from,
                                    subject: subject,
                                    filename: `${message.id}.pdf`,
                                    path: `${config_1.default.get('general.outputFolder')}${message.id}.pdf`,
                                    message: resultMessage
                                });
                            }).then(() => {
                                resolve(null);
                            });
                        });
                        pdfPromises.push(pdfPromise);
                    }
                }
                // Once everything has has been sent, clean up.
                Promise.all(pdfPromises).then(() => {
                    log('Sent! Cleaning up....', log_1.INFO);
                    // Clear inbox
                    imapFunctions.emptyInbox();
                    // Clear attachments
                    emptyFolder(config_1.default.get('general.attachmentFolder'));
                    // Clear merged
                    emptyFolder(config_1.default.get('general.outputFolder'));
                    log(`${emailsProcessed} ${emailsProcessed === 1 ? 'email' : 'emails'} processed.`, log_1.INFO);
                });
            }
            restartLoop();
        });
        // Run once, then set interval
        yield mainLoop();
    });
}
main();
//# sourceMappingURL=app.js.map