"use strict";
/*
Email PDF Combiner
Jack Romano (c) 2021
*/
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer_1 = __importDefault(require("nodemailer"));
class SmtpFunctions {
    constructor(options) {
        this.config = {
            smtpPort: options.port,
            smtpHost: options.host,
            address: options.address,
            password: options.password
        };
        this.smtpConnection = nodemailer_1.default.createTransport({
            host: this.config.smtpHost,
            port: this.config.smtpPort,
            secure: true,
            auth: {
                user: this.config.address,
                pass: this.config.password
            }
        });
    }
    sendEmail(messageDetails) {
        return __awaiter(this, void 0, void 0, function* () {
            let attachments;
            if (!messageDetails.filename) {
                attachments = null;
            }
            else {
                attachments = [
                    {
                        filename: messageDetails.filename,
                        path: messageDetails.path
                    }
                ];
            }
            // send mail with defined transport object
            const info = yield this.smtpConnection.sendMail({
                from: this.config.address,
                to: messageDetails.to,
                subject: messageDetails.subject,
                text: `Merged PDF attached.\n${messageDetails.message || ''}`,
                attachments
            });
        });
    }
}
exports.default = SmtpFunctions;
//# sourceMappingURL=SmtpFunctions.js.map