"use strict";
/*
Email PDF Combiner
Jack Romano (c) 2021
*/
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const imap_simple_1 = __importDefault(require("imap-simple"));
const sanitize_filename_1 = __importDefault(require("sanitize-filename"));
const uuid_1 = require("uuid");
const fs_1 = __importDefault(require("fs"));
const log_1 = require("./log");
class ImapFunctions {
    constructor(options, log) {
        this.config = {
            imapConfig: {
                imap: {
                    user: options.address,
                    password: options.password,
                    host: options.server,
                    port: options.port,
                    tls: true,
                    authTimeout: 5000
                }
            },
            attachmentFolder: options.attachmentFolder,
            validExtensions: options.validExtensions,
            codeOn: options.codeOn,
            code: options.code
        };
        this.log = log;
    }
    static toUpper(thing) { return thing && thing.toUpperCase ? thing.toUpperCase() : thing; }
    fetchEmail() {
        return __awaiter(this, void 0, void 0, function* () {
            const messageDetails = [];
            const result = new Promise((resolve) => {
                imap_simple_1.default.connect(this.config.imapConfig).then(connection => {
                    connection.openBox('INBOX').then(function () {
                        // Fetch emails
                        const searchCriteria = ['ALL'];
                        const fetchOptions = {
                            bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)'],
                            struct: true
                        };
                        // retrieve only the headers of the messages
                        return connection.search(searchCriteria, fetchOptions);
                    }).then(messages => {
                        let attachments = [];
                        messages.forEach(message => {
                            const id = (0, uuid_1.v4)();
                            const parts = imap_simple_1.default.getParts(message.attributes.struct);
                            attachments = attachments.concat(parts.filter(function (part) {
                                return part.disposition && part.disposition.type.toUpperCase() ===
                                    'ATTACHMENT';
                            }).map(function (part) {
                                // retrieve the attachments only of the messages with attachments
                                return connection.getPartData(message, part)
                                    .then(function (partData) {
                                    return {
                                        id,
                                        filename: part.disposition.params.filename,
                                        data: partData,
                                        encoding: part.encoding,
                                        subtype: part.subtype
                                    };
                                });
                            }));
                            // If code is ON, remove messages lacking the proper code
                            if (this.config.codeOn && !message.parts[0].body.subject[0].includes(this.config.code)) {
                                this.log('Code is required, but not provided in this message. Deleting...', log_1.ERROR);
                                this.deleteEmail(connection, message.attributes.uid);
                            }
                            else {
                                // Push message details
                                messageDetails.push({
                                    id,
                                    from: message.parts[0].body.from[0],
                                    subject: message.parts[0].body.subject[0]
                                });
                            }
                        });
                        return Promise.all(attachments);
                    }).then(attachments => {
                        attachments.forEach(attachment => {
                            // only download if PDF
                            if (this.config.validExtensions.some(extension => attachment.filename.includes(extension))) {
                                // Create a write stream so that we can stream the attachment to file;
                                const nameFilename = `${this.config.attachmentFolder}${(0, sanitize_filename_1.default)(attachment.filename)}`;
                                const fileDescriptor = fs_1.default.openSync(nameFilename, 'w');
                                fs_1.default.writeSync(fileDescriptor, attachment.data);
                                fs_1.default.closeSync(fileDescriptor);
                            }
                        });
                        return attachments.map(attachment => ({
                            id: attachment.id,
                            filename: `${this.config.attachmentFolder}${(0, sanitize_filename_1.default)(attachment.filename)}`
                        }));
                    }).then(attachments => {
                        connection.closeBox(true);
                        connection.end();
                        resolve({ messages: messageDetails, attachments });
                    });
                });
            });
            return result;
        });
    }
    deleteEmail(connection, uid) {
        connection.addFlags(uid, '\\Deleted', err => {
            this.log('Message deleted.' + err, log_1.WARN);
        });
    }
    emptyInbox() {
        imap_simple_1.default.connect(this.config.imapConfig).then(connection => {
            connection.openBox('INBOX').then(function () {
                const searchCriteria = ['ALL'];
                const fetchOptions = { bodies: ['TEXT'], struct: true };
                return connection.search(searchCriteria, fetchOptions);
                // Loop over each message
            }).then(messages => {
                const taskList = messages.map(message => {
                    return new Promise((resolve, reject) => {
                        const parts = imap_simple_1.default.getParts(message.attributes.struct);
                        parts.map(part => {
                            return connection.getPartData(message, part)
                                .then(partData => {
                                // Mark message for deletion
                                connection.addFlags(message.attributes.uid, '\\Deleted', (err) => {
                                    if (err) {
                                        this.log('Problem marking message for deletion', log_1.ERROR);
                                        reject(err);
                                    }
                                    resolve(null); // Final resolve
                                });
                            });
                        });
                    });
                });
                return Promise.all(taskList).then(() => {
                    connection.imap.closeBox(true, (err) => {
                        if (err) {
                            this.log(`Error closing inbox: ${err}`, log_1.ERROR);
                        }
                    });
                    connection.end();
                });
            });
        });
    }
}
exports.default = ImapFunctions;
//# sourceMappingURL=ImapFunctions.js.map