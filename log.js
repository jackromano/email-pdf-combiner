"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UPDATE = exports.INFO = exports.DEBUG = exports.ERROR = exports.WARN = void 0;
const chalk_1 = __importDefault(require("chalk"));
exports.WARN = 'warn';
exports.ERROR = 'error';
exports.DEBUG = 'debug';
exports.INFO = 'info';
exports.UPDATE = 'update';
// export const log = (message, level = DEBUG) => {
//   switch (level) {
//     case ERROR:
//       console.log(chalk.red(message))
//       break
//     case WARN:
//       console.log(chalk.yellow(message))
//       break
//     case INFO:
//       console.log(chalk.white(message))
//       break
//     case DEBUG:
//       console.log(chalk.green(message))
//       break
//     case UPDATE:
//       process.stdout.write('\r' + chalk.bgWhite.black(message))
//       break
//     default:
//       console.log(chalk.green(message))
//   }
// }
class Log {
    constructor() {
        this.lastUpdate = false;
        this.log = (message, level = exports.DEBUG) => {
            if (level !== exports.UPDATE && this.lastUpdate) {
                console.log('\n');
                this.lastUpdate = false;
            }
            switch (level) {
                case exports.ERROR:
                    console.log(chalk_1.default.red(message));
                    break;
                case exports.WARN:
                    console.log(chalk_1.default.yellow(message));
                    break;
                case exports.INFO:
                    console.log(chalk_1.default.white(message));
                    break;
                case exports.DEBUG:
                    console.log(chalk_1.default.green(message));
                    break;
                case exports.UPDATE:
                    this.lastUpdate = true;
                    process.stdout.write('\r' + chalk_1.default.bgWhite.black(message));
                    break;
                default:
                    console.log(chalk_1.default.green(message));
            }
        };
    }
}
exports.default = Log;
//# sourceMappingURL=log.js.map