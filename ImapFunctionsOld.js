"use strict";
/*
Email PDF Combiner
Jack Romano (c) 2021
*/
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const imap_1 = __importDefault(require("imap"));
const base64_stream_1 = require("base64-stream");
const util_1 = require("util");
class ImapFunctions {
    constructor(options = {}) {
        this.options = options;
    }
    static toUpper(thing) { return thing && thing.toUpperCase ? thing.toUpperCase() : thing; }
    findAttachmentParts(struct, attachments = []) {
        for (let i = 0, len = struct.length; i < len; ++i) {
            if (Array.isArray(struct[i])) {
                this.findAttachmentParts(struct[i], attachments);
            }
            else {
                if (struct[i].disposition && ['INLINE', 'ATTACHMENT'].indexOf(ImapFunctions.toUpper(struct[i].disposition.type)) > -1) {
                    attachments.push(struct[i]);
                }
            }
        }
        return attachments;
    }
    buildAttMessageFunction(attachment, filenamePromise, error) {
        const filename = `${this.options.attachmentFolder}${attachment.params.name}`;
        const encoding = attachment.encoding;
        return (msg, sequenceNumber) => {
            const prefix = '(#' + sequenceNumber + ') ';
            msg.on('error', error);
            msg.on('body', function (stream, info) {
                // Create a write stream so that we can stream the attachment to file;
                console.log(prefix + 'Streaming this attachment to file', filename, info);
                const writeStream = fs_1.default.createWriteStream(filename);
                writeStream.on('finish', function () {
                    console.log(prefix + 'Done writing to file %s', filename);
                });
                // stream.pipe(writeStream); this would write base64 data to the file.
                // so we decode during streaming using
                if (ImapFunctions.toUpper(encoding) === 'BASE64') {
                    // the stream is base64 encoded, so here the stream is decode on the fly and piped to the write stream (file)
                    stream.pipe(new base64_stream_1.Base64Decode()).pipe(writeStream);
                }
                else {
                    // here we have none or some other decoding streamed directly to the file which renders it useless probably
                    stream.pipe(writeStream);
                }
            });
            msg.once('end', function () {
                console.log(prefix + 'Finished attachment %s', filename);
                filenamePromise(filename);
            });
        };
    }
    // Fetch Email and Download Attachments, if any
    fetchEmail() {
        const imap = new imap_1.default({
            user: this.options.address,
            password: this.options.password,
            host: this.options.server,
            port: this.options.port,
            tls: true
        });
        const openInbox = (cb) => {
            imap.openBox('INBOX', true, cb);
        };
        imap.once('ready', () => {
            openInbox((err, box) => {
                if (err)
                    throw err;
                const f = imap.seq.fetch(box.messages.total + ':*', {
                    bodies: 'HEADER.FIELDS (FROM TO SUBJECT DATE)',
                    struct: true
                });
                f.on('message', (msg, sequenceNumber) => {
                    console.log('Message #%d', sequenceNumber);
                    const prefix = '(#' + sequenceNumber + ') ';
                    msg.on('body', (stream) => {
                        let buffer = '';
                        stream.on('data', (chunk) => {
                            buffer += chunk.toString();
                        });
                        stream.once('end', () => {
                            console.log(prefix + 'Parsed header: %s', (0, util_1.inspect)(imap_1.default.parseHeader(buffer)));
                        });
                    });
                    msg.once('attributes', (attrs) => {
                        // console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8))
                        const attachments = this.findAttachmentParts(attrs.struct);
                        console.log(prefix + 'Has attachments: %d', attachments.length);
                        const filenamePromises = [];
                        for (let i = 0, len = attachments.length; i < len; ++i) {
                            const attachment = attachments[i];
                            console.log(prefix + 'Fetching attachment %s', attachment.params.name);
                            const f = imap.fetch(attrs.uid, {
                                bodies: [attachment.partID],
                                struct: true
                            });
                            // build function to process attachment message
                            filenamePromises.push(new Promise((resolve, reject) => {
                                f.on('message', this.buildAttMessageFunction(attachment, resolve, reject));
                            }));
                        }
                        Promise.all(filenamePromises).then((fileNames) => {
                            //mergePDFs(fileNames, OUTPUT_FOLDER + '/output.pdf').then(() => {
                            // send message
                        });
                    });
                });
                msg.once('end', () => {
                    console.log(prefix + 'Finished');
                });
            });
            f.once('error', err => {
                console.log('Fetch error: ' + err);
            });
            f.once('end', () => {
                console.log('Done fetching all messages!');
                imap.end();
            });
        });
    }
    once() { }
    function(err) {
        console.log(err);
    }
    once() { }
    function() {
        console.log('Connection ended');
    }
}
exports.default = ImapFunctions;
//# sourceMappingURL=ImapFunctionsOld.js.map