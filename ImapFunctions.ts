/*
Email PDF Combiner
Jack Romano (c) 2021
*/

import imaps from 'imap-simple'
import sanitize from 'sanitize-filename'
import { v4 as uuidv4 } from 'uuid'
import fs from 'fs'
import { WARN, ERROR } from './log'

class ImapFunctions {
  private config
  readonly log

  constructor (options, log) {
    this.config = {
      imapConfig: {
        imap: {
          user: options.address,
          password: options.password,
          host: options.server,
          port: options.port,
          tls: true,
          authTimeout: 5000
        }
      },
      attachmentFolder: options.attachmentFolder,
      validExtensions: options.validExtensions,
      codeOn: options.codeOn,
      code: options.code
    }
    this.log = log
  }

  private static toUpper (thing) { return thing && thing.toUpperCase ? thing.toUpperCase() : thing }

  public async fetchEmail () {
    const messageDetails = []

    const result = new Promise((resolve) => {
      imaps.connect(this.config.imapConfig).then(connection => {
        connection.openBox('INBOX').then(function () {
          // Fetch emails
          const searchCriteria = ['ALL']
          const fetchOptions = {
            bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)'],
            struct: true
          }

          // retrieve only the headers of the messages
          return connection.search(searchCriteria, fetchOptions)
        }).then(messages => {
          let attachments = []

          messages.forEach(message => {
            const id = uuidv4()
            const parts = imaps.getParts(message.attributes.struct)
            attachments = attachments.concat(parts.filter(function (part) {
              return part.disposition && part.disposition.type.toUpperCase() ===
                'ATTACHMENT'
            }).map(function (part) {
              // retrieve the attachments only of the messages with attachments
              return connection.getPartData(message, part)
                .then(function (partData) {
                  return {
                    id,
                    filename: part.disposition.params.filename,
                    data: partData,
                    encoding: part.encoding,
                    subtype: part.subtype
                  }
                })
            }))

            // If code is ON, remove messages lacking the proper code
            if (this.config.codeOn && !message.parts[0].body.subject[0].includes(this.config.code)) {
              this.log('Code is required, but not provided in this message. Deleting...', ERROR)
              this.deleteEmail(connection, message.attributes.uid)
            } else {
              // Push message details
              messageDetails.push({
                id,
                from: message.parts[0].body.from[0],
                subject: message.parts[0].body.subject[0]
              })
            }
          })

          return Promise.all(attachments)
        }).then(attachments => {
          attachments.forEach(attachment => {
            // only download if PDF
            if (this.config.validExtensions.some(extension => attachment.filename.includes(extension))) {
              // Create a write stream so that we can stream the attachment to file;
              const nameFilename = `${this.config.attachmentFolder}${sanitize(attachment.filename)}`
              const fileDescriptor = fs.openSync(nameFilename, 'w')
              fs.writeSync(fileDescriptor, attachment.data)
              fs.closeSync(fileDescriptor)
            }
          })
          return attachments.map(attachment => ({
            id: attachment.id,
            filename: `${this.config.attachmentFolder}${sanitize(attachment.filename)}`
          }))
        }).then(attachments => {
          connection.closeBox(true)
          connection.end()
          resolve({ messages: messageDetails, attachments })
        })
      })
    })
    return result
  }

  private deleteEmail (connection, uid) {
    connection.addFlags(uid, '\\Deleted', err => {
      this.log('Message deleted.' + err, WARN)
    })
  }

  public emptyInbox () {
    imaps.connect(this.config.imapConfig).then(connection => {
      connection.openBox('INBOX').then(function () {
        const searchCriteria = ['ALL']
        const fetchOptions = { bodies: ['TEXT'], struct: true }
        return connection.search(searchCriteria, fetchOptions)

        // Loop over each message
      }).then(messages => {
        const taskList = messages.map(message => {
          return new Promise((resolve, reject) => {
            const parts = imaps.getParts(message.attributes.struct)
            parts.map(part => {
              return connection.getPartData(message, part)
                .then(partData => {
                  // Mark message for deletion
                  connection.addFlags(message.attributes.uid, '\\Deleted', (err) => {
                    if (err) {
                      this.log('Problem marking message for deletion', ERROR)
                      reject(err)
                    }

                    resolve(null) // Final resolve
                  })
                })
            })
          })
        })

        return Promise.all(taskList).then(() => {
          connection.imap.closeBox(true, (err) => {
            if (err) {
              this.log(`Error closing inbox: ${err}`, ERROR)
            }
          })
          connection.end()
        })
      })
    })
  }
}

export default ImapFunctions
