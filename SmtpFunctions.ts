/*
Email PDF Combiner
Jack Romano (c) 2021
*/

import nodemailer from 'nodemailer'

class SmtpFunctions {
  private config
  // private sendmail
  private smtpConnection

  constructor (options) {
    this.config = {
      smtpPort: options.port,
      smtpHost: options.host,
      address: options.address,
      password: options.password
    }

    this.smtpConnection = nodemailer.createTransport({
      host: this.config.smtpHost,
      port: this.config.smtpPort,
      secure: true,
      auth: {
        user: this.config.address,
        pass: this.config.password
      }
    })
  }

  public async sendEmail (messageDetails) {
    let attachments
    if (!messageDetails.filename) {
      attachments = null
    } else {
      attachments = [
        {
          filename: messageDetails.filename,
          path: messageDetails.path
        }
      ]
    }

    // send mail with defined transport object
    const info = await this.smtpConnection.sendMail({
      from: this.config.address,
      to: messageDetails.to,
      subject: messageDetails.subject,
      text: `Merged PDF attached.\n${messageDetails.message || ''}`,
      attachments
    })
  }
}

export default SmtpFunctions
