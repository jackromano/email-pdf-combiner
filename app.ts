/*
Email PDF Combiner
Jack Romano (c) 2021
*/

import PDFMerge from 'pdf-merge'
import config from 'config'
import ImapFunctions from './ImapFunctions'
import SmtpFunctions from './SmtpFunctions'
import fs from 'fs'
import path from 'path'
import docxConverter from 'docx-pdf'
import chalk from 'chalk'
import Log, { WARN, ERROR, INFO, UPDATE, DEBUG } from './log'

async function main () {
  let intervalID
  const Logger = new Log()
  const log = Logger.log

  console.log(chalk.bgWhite.black(`
╔════════════════════════╗
║   ${chalk.bold('Email PDF Combiner')}   ║
║  Jack Romano (c) 2022  ║
╚════════════════════════╝\n
`))

  if (config.get('general.codeOn')) {
    log(`${chalk.red('SECURITY CODE ON!')} For security, only emails with the code "${chalk.red(config.get('email.code'))}" in the subject line will be processed. \n${chalk.dim('To disable this feature, set "codeOn" to ' + chalk.italic('false') + ' in default.json.\n')}`)
  }

  // Handle SIGINT
  process.on('SIGINT', () => {
    log('Closing...', WARN)
    clearInterval(intervalID)
  })

  const imapFunctions = new ImapFunctions({
    address: config.get('email.address'),
    password: config.get('email.password'),
    server: config.get('imap.server'),
    port: config.get('imap.port'),
    attachmentFolder: config.get('general.attachmentFolder'),
    validExtensions: config.get('general.validExtensions'),
    codeOn: config.get('general.codeOn'),
    code: config.get('email.code')
  }, log)

  const smtpFunctions = new SmtpFunctions({
    host: config.get('smtp.server'),
    port: config.get('smtp.port'),
    address: config.get('email.address'),
    password: config.get('email.password')
  })

  const emailDelay = config.get('general.checkEmailDelay') * 1000

  const restartLoop = () => {
    intervalID = setTimeout(mainLoop, emailDelay)
  }

  const emptyFolder = folderPath => {
    fs.readdir(folderPath, (err, files) => {
      if (err) throw err

      for (const file of files) {
        fs.unlink(path.join(folderPath, file), err => {
          if (err) throw err
        })
      }
    })
  }

  const mergePDFs = async (
    files: Array<string>, fileName: string): Promise<void> => {
    return PDFMerge(files, { output: fileName })
  }

  const handleDocx = (resolve, reject) => (err, result) => {
    if (err) {
      log('Error converting DOCX to PDF.', ERROR)
      reject(err)
    } else {
      log('Converted DOCX to PDF.', INFO)
      resolve(result)
    }
  }

  // Main Loop
  const mainLoop = async () => {
    // Check email

    const messagesData: any = await imapFunctions.fetchEmail()

    const emailsProcessed = messagesData.messages.length
    if (emailsProcessed === 0) {
      const now = new Date()
      log(` [${now.toLocaleTimeString()}] No emails found. Waiting... `, UPDATE)
    } else {
      log(`${messagesData.messages.length} ${messagesData.messages.length === 1 ? 'message' : 'messages'} received.`, INFO)
      const pdfPromises = []
      for (let index = 0; index < messagesData.messages.length; index++) {
        let resultMessage = ''
        const message = messagesData.messages[index]
        const pdfsToMerge = messagesData.attachments.filter(attachment => {
          return attachment.id === message.id &&
            config.get('general.validExtensions')
              .some(extension => attachment.filename.includes(extension))
        }).map(attachment => attachment.filename)

        // Handle emails with no attachments.
        if (pdfsToMerge.length === 0) {
          log('No attachments found in email. Sending email and skipping', WARN)
          await smtpFunctions.sendEmail({
            to: message.from,
            subject: `No Attachments: ${message.subject}`,
            message: 'No attachments were found in this email.'
          })
        } else {
          for (let index = 0; index < pdfsToMerge.length; index++) {
          // pdfsToMerge.forEach((attachmentFilename, index) => {
            const attachmentFilename = pdfsToMerge[index]
            if (attachmentFilename &&
                attachmentFilename.includes('.docx')) {
              const newFilename = `${attachmentFilename}.pdf`
              // convert the docx to pdf
              await new Promise((resolve, reject) => {
                docxConverter(attachmentFilename,
                  newFilename,
                  handleDocx(resolve, reject))
              })
              // replace  the docx filename with the pdf name
              pdfsToMerge[index] = newFilename
              resultMessage += `\nConverted ${attachmentFilename} to PDF.`
            }
          }

          // Handle emails with attachments
          log(
            `Merging ${pdfsToMerge.length} files in email ${index +
            1} of ${emailsProcessed}`, INFO)
          resultMessage += `\nMerged ${pdfsToMerge.length} files in email.`

          // Merge PDFs
          const pdfPromise = new Promise((resolve, reject) => {
            mergePDFs(
              pdfsToMerge,
              `${config.get('general.outputFolder')}${message.id}.pdf`
            ).then(() => {
              // Send PDF
              const subject = `MERGED: ${message.subject}`
              log(`Sending message ${index +
              1} of ${emailsProcessed} (${chalk.italic(subject)})...`, INFO)
              return smtpFunctions.sendEmail({
                to: message.from,
                subject: subject,
                filename: `${message.id}.pdf`,
                path: `${config.get('general.outputFolder')}${message.id}.pdf`,
                message: resultMessage
              })
            }).then(() => {
              resolve(null)
            })
          })
          pdfPromises.push(pdfPromise)
        }
      }

      // Once everything has has been sent, clean up.
      Promise.all(pdfPromises).then(() => {
        log('Sent! Cleaning up....', INFO)
        // Clear inbox
        imapFunctions.emptyInbox()
        // Clear attachments
        emptyFolder(config.get('general.attachmentFolder'))
        // Clear merged
        emptyFolder(config.get('general.outputFolder'))

        log(`${emailsProcessed} ${emailsProcessed === 1 ? 'email' : 'emails'} processed.`, INFO)
      })
    }
    restartLoop()
  }

  // Run once, then set interval
  await mainLoop()
}

main()
